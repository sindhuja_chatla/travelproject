// import React from 'react';
// import './SearchBar.css'; // Make sure the path to your CSS file is correct

// export default function SearchBar() {
//   return (
//     <div className='container'>
//       <div className='bimg'>

//       <div className='homecard grid'>

// <div className='locationDiv'>
//     <label htmlFor="location">Location</label>
//     <input type="text" placeholder="Dream Destination" />
// </div>
// <div className='distDiv'>
//     <label htmlFor="distance">Distance</label>
//     <input type="text" placeholder="11/Meters" />
// </div>
// <div className='priceDiv'>
//     <label htmlFor="price">Price</label>
//     <input type="text" placeholder="$140-$500" />
// </div>
// <button>Search</button>
//       </div>
//       </div>
//     </div>
//   );
// }


// import React from 'react';
// import './SearchBar.css'; // Make sure the path to your CSS file is correct

// export default function SearchBar() {
//   return (
//     <div className='container'>
//       <div className='bimg'>
//         <div className='homecard grid'>
//           <div className='locationDiv'>
//             <label htmlFor="location">Location</label>
//             <input type="text" placeholder="Dream Destination" />
//           </div>
//           <div className='distDiv'>
//             <label htmlFor="distance">Distance</label>
//             <input type="text" placeholder="11/Meters" />
//           </div>
//           <div className='priceDiv'>
//             <label htmlFor="price">Price</label>
//             <input type="text" placeholder="$140-$500" />
//           </div>
//           <button>Search</button>
//         </div>
//       </div>
//     </div>
//   );
// }


// import React from 'react';
// import './SearchBar.css'; // Make sure the path to your CSS file is correct
// import {Button} from 'reactstrap'

// export default function SearchBar() {
//   return (
//     <div className='container'>
//       <div className='bimg'>
//         <div className='homecard grid'>
//           <div className='locationDiv'>
//             <label htmlFor="location">Location</label>
//             <input type="text" placeholder="Dream Destination" />
//           </div>
//           <div className='distDiv'>
//             <label htmlFor="distance">Distance</label>
//             <input type="text" placeholder="11/Meters" />
//           </div>
//           <div className='priceDiv'>
//             <label htmlFor="price">Price</label>
//             <input type="text" placeholder="$140-$500" />
//           </div>
//           <Button className='searchbar'>Search</Button>
//         </div>
//       </div>
//     </div>
//   );
// }


import React from 'react';
import './SearchBar.css'; 
import { Button } from 'reactstrap';
import bgvideo2 from './bgvideo2.mp4';

export default function SearchBar() {
  return (
    <div className='container'>
      <div className='bgvideo'>
        <video autoPlay muted loop className='bg-vid' >
        <source src={bgvideo2} type='video/mp4' /> 
        </video>
       
<div className="bg-text1">
<h1 >EXPLORE YOUR DREAM </h1>
</div>
<div className="bg-text2">
<h1>DESTINATION</h1>
</div>

        <div className='homecard grid'>
          <div className='locationDiv'>
            <label htmlFor='location'>Location</label>
            <input type='text' placeholder='Dream Destination' />
          </div>
          <div className='distDiv'>
            <label htmlFor='distance'>Check-in Date</label>
            {/* <input type='text' placeholder='11/Meters' /> */}
            {/* <div class="date-picker"> */}<input type="date" placeholder="DD/MM/YYYY"/>
     {/* <input  type="text" id="checkInDate" placeholder="Check-in Date" readonly/> */}
  {/* </div> */}
          </div>
          <div className='priceDiv'>
            <label htmlFor='price'>Price</label>
            <input type='text' placeholder='$140-$500' />
          </div>
          <Button className='searchbar'>Search</Button>
        </div>
      </div>
    </div>
  );
}